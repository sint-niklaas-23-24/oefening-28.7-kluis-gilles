﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_28._7___Kluis
{
    internal class DigitaleKluis
    {
        private int _code;
        private bool _kanCodeTonen;
        private int _codeLevel;
        private int _aantalPogingen;

        public DigitaleKluis() { }
        public DigitaleKluis(int code)
        {
            this.Code = code;
            KanCodeTonen = false;
            _codeLevel = code / 1000;
            _aantalPogingen = 0;
        }

        private int Code
        { 
            get 
            {
                int temp = -666;
                if (KanCodeTonen == true) 
                {
                    temp = _code;
                } 
                return temp;
            }
            set { _code = value; }
        }
        public bool KanCodeTonen
        {
            get { return _kanCodeTonen; }
            set { _kanCodeTonen = value; }
        }
        public int CodeLevel
        {
            get { return _codeLevel; }
        }

        public bool TryCode(int code)
        {
            bool resultaat = false;
            _aantalPogingen++;
            if (code == this._code)
            {
                resultaat = true;
            }
            return resultaat;
        }
        public static string BruteforceCode(DigitaleKluis eenKluis)
        {
            int teller = 0;
            while (eenKluis.TryCode(teller) == false)
            {
                teller++;
            }
            return $"De code werd gekraakt!{Environment.NewLine}Aantal pogingen: {eenKluis._aantalPogingen}" + Environment.NewLine + "Code: -666";
        }
    }
}
