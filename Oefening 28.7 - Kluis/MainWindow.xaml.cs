﻿using System;
using System.Media;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Oefening_28._7___Kluis
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        DigitaleKluis kluis = new DigitaleKluis(10);
        SoundPlayer soundPlayer = new SoundPlayer(@"Audio/fbi_openup.wav");
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            lblPogingen.Content = "Aantal pogingen: 0";
            imgKluis.Source = new BitmapImage(new Uri("Afbeeldingen/kluis_gesloten.JPG", UriKind.Relative));
        }
        int teller = 0;
        private void btnInvoeren_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(txtCodeInvoer.Text))
            {
                if (int.TryParse(txtCodeInvoer.Text, out int resultaat) == true)
                {
                    teller++;
                    kluis.KanCodeTonen = kluis.TryCode(resultaat);

                    if (kluis.KanCodeTonen == true)
                    {
                        imgKluis.Source = new BitmapImage(new Uri("Afbeeldingen/kluis_open.JPG", UriKind.Relative));
                        btnInvoeren.IsEnabled = false;
                        lblPogingen.Content = $"Je hebt de code gevonden! Aantal pogingen: {teller}";
                        soundPlayer.Play();
                        teller = 0;
                    }
                    else if (resultaat == -666)
                    {
                        lblPogingen.Content = "Je bent een cheater";
                    }
                    else
                    {
                        lblPogingen.Content = $"Aantal pogingen: {teller}";
                    }
                }
            }
        }

        private void btnKraakCode_Click(object sender, RoutedEventArgs e)
        {
            lblPogingen.Content = DigitaleKluis.BruteforceCode(kluis);
            imgKluis.Source = new BitmapImage(new Uri("Afbeeldingen/kluis_open.JPG", UriKind.Relative));
            soundPlayer.Play();
        }
    }
}
